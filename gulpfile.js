'use strict'

let gulp = require('gulp')
let mocha = require('gulp-mocha')
let env = require('gulp-env')
let nodemon = require('gulp-nodemon')
let plumber = require('gulp-plumber')
let jshint = require('gulp-jshint')
let jsonlint = require('gulp-json-lint')
let standard = require('gulp-standard')

let paths = {
  js: ['*.js', '*/*.js', '*/**/*.js', '!node_modules/**', '!gulpfile.js'],
  json: ['*.json', '*/*.json', '*/**/*.json', '!node_modules/**'],
  tests: ['./tests/*.js']
}

gulp.task('jslint', function () {
  return gulp.src(paths.js)
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'))
})

gulp.task('jsonlint', function () {
  return gulp.src(paths.json)
    .pipe(plumber())
    .pipe(jsonlint({
      comments: true
    }))
    .pipe(jsonlint.report())
})

gulp.task('standard', function () {
  return gulp.src(['./app.js'])
    .pipe(plumber())
    .pipe(standard())
    .pipe(standard.reporter('default', {
      breakOnError: true,
      quiet: true
    }))
})

gulp.task('run-tests', function () {
  require('dotenv').config()

  env({
    vars: {
      NODE_ENV: 'test'
    }
  })

  return gulp
    .src(paths.tests)
    .pipe(plumber())
    .pipe(mocha({
      reporter: 'spec',
      exit: true
    }))
})

let runApp = function () {
  require('dotenv').config()

  env({
    vars: {
      NODE_ENV: 'development'
    }
  })

  nodemon({
    script: 'index.js',
    ext: 'js',
    watch: paths.js,
    ignore: [
      'node_modules',
      '.git',
      '.gitignore',
      '.gitlab-ci.yml',
      'Dockerfile',
      '.dockerignore',
      '.jshintrc',
      'build.bat',
      'build.sh',
      'specs/**',
      'k8s/**'
    ],
    restartable: 'rs'
  })
}

gulp.task('run', () => runApp())
gulp.task('lint', ['jslint', 'jsonlint', 'standard'])
gulp.task('test', ['lint', 'run-tests'])
gulp.task('default', ['run'])

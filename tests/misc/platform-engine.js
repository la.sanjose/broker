'use strict'

const BPromise = require('bluebird')
const EventEmitter = require('events').EventEmitter

const Broker = require('../../node_modules/broker-logger/lib/broker.js')

class DemoPlatformEngine extends EventEmitter {
  constructor () {
    super()

    this.broker = new Broker()
    this.BROKER = process.env.BROKER || 'amqp://guest:guest@localhost/'

    this.safeParse = BPromise.method(JSON.parse)
  }

  init () {
    return this.broker.connect(this.BROKER)
      .then(() => this.broker.createQueue('logs'))
      .then(() => this.broker.createQueue('exceptions'))
      .then(() => this.broker.createQueue('logger1'))
      .then(() => this.broker.createQueue('logger2'))
      .then(() => this.broker.createQueue('exlogger1'))
      .then(() => this.broker.createQueue('exlogger2'))
      .then(() => this.broker.queues.logs.consume(this.genericListener.bind(this)))
      .then(() => this.broker.queues.exceptions.consume(this.genericListener.bind(this)))
      .then(() => this.broker.queues.logger1.consume(this.genericListener.bind(this)))
      .then(() => this.broker.queues.logger2.consume(this.genericListener.bind(this)))
      .then(() => this.broker.queues.exlogger1.consume(this.genericListener.bind(this)))
      .then(() => this.broker.queues.exlogger2.consume(this.genericListener.bind(this)))
  }

  genericListener (msg) {
    return this.safeParse(msg.content.toString()).then(content => {
      let returnData = {
        routingKey: msg.fields.routingKey,
        content
      }
      let action = 'log'
      if (msg.fields.routingKey.indexOf('ex') > -1) action = 'exception'
      this.emit(action, returnData)
      return true
    }).catch(console.error)
  }
}

module.exports = DemoPlatformEngine

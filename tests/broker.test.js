/* global describe, it, before, after */
'use strict'

const { exec } = require('child_process')
let BPromise = require('bluebird')
global.Promise = BPromise

// let Package = require('../index')
const DemoPlatformEngine = require('./misc/platform-engine')
// const TestData = require('./utils/data-samples')
// const BROKER = 'amqp://guest:guest@localhost'

function promiseFromChildProcess(child) {
  return new Promise((resolve, reject) => {
    child.addListener("error", reject)
    child.addListener("exit", resolve)
  })
}

describe('Package Test', () => {
  // let _config = {}
  // let testResult = {}
  let demoEngine = null

  before('#Test init', (done) => {
    demoEngine = new DemoPlatformEngine()
    demoEngine.init()
      // .catch(console.error)
      .then(() => {
        // Create the fifo files
        let logChild = exec('mkfifo ' + process.env.FIFO_PATH_LOG)
        return promiseFromChildProcess(logChild)
          .then(() => {
            let exlogChild = exec('mkfifo ' + process.env.FIFO_PATH_EXCEPTION_LOG)
            return promiseFromChildProcess(exlogChild)
          })
          .catch(err => {
            console.error(err)
          })
      })
      .then(() => {
        return done()
      })
      .catch(err => {
        console.error(err)
      })
  })

  after('#Clean up', (done) => {
    // TODO
    let logChild = exec('rm -f ' + process.env.FIFO_PATH_LOG)
    promiseFromChildProcess(logChild)
      .then(() => {
        let exlogChild = exec('rm -f ' + process.env.FIFO_PATH_EXCEPTION_LOG)
        return promiseFromChildProcess(exlogChild)
      })
      .then(() => {
        return done()
      })
      .catch(err => {
        console.error(err)
      })
  })

  describe('#init', () => {
    it('should send log', function (done) {
      this.timeout(10000)

      done()
      // TODO: pust text in fifo
      /*testResult.log = 0

      console.log(process.cwd())
      let logChild = fork(process.cwd() + '/tests/utils/run.js')
      promiseFromChildProcess(logChild)
        .then((result) => {
          console.log('result? ', result)
        })
        .catch(err => console.error)
      let cmdStr = 'echo ' + TestData.testCase[0].testMsg + ' > ' + process.env.FIFO_PATH_LOG
      console.log('... ', cmdStr)
      demoEngine.on('log', (data) => {
        console.log('log!!!! ', data)
        if (
          data.routingKey === 'logs' &&
          JSON.stringify(data.content.data) === JSON.stringify(TestData.testCase[0].data)
        ) testResult.log += 1
        if (
          data.routingKey !== 'logs' &&
          TestData.testCase[0].actualQueues.indexOf(data.routingKey) > -1 &&
          JSON.stringify(data.content) === JSON.stringify(TestData.testCase[0].data)
        ) testResult.log += 1
        if (testResult.log === 3) done()
      })
    })*/
    })
  })
})

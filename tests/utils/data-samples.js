'use strict'

const LOGGERS = 'logger1,logger2'
const EXCEPTIONS = 'exlogger1,exlogger2'

exports.loggers = LOGGERS
exports.exceptions = EXCEPTIONS

exports.testCase = [
  {
    actualQueues: ['logs'].concat(LOGGERS.split(',')),
    testMsg: 'sample',
    data: {
      title: 'sample',
      device: {
        name: 'sample'
      }
    }
  },
  {
    actualQueues: ['exceptions'].concat(EXCEPTIONS.split(',')),
    testMsg: 'sampleError',
    data: {
      msg: 'the sample error'
    }
  }
]

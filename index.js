'use strict'

const fs = require('fs')
const net = require('net')
const _ = require('lodash')
const BPromise = require('bluebird')
const BrokerLogger = require('broker-logger')
global.Promise = BPromise

// Required env var
const config = {}
config.BROKER = process.env.BROKER
config.ACCOUNT = process.env.ACCOUNT
config.PLUGIN_ID = process.env.PLUGIN_ID
config.PLUGIN_TYPE = process.env.PLUGIN_TYPE
config.FIFO_PATH_LOG = process.env.FIFO_PATH_LOG
config.FIFO_PATH_EXCEPTION_LOG = process.env.FIFO_PATH_EXCEPTION_LOG
// Optional env var
config.LOGGERS = ''
config.EXCEPTION_LOGGERS = ''

let brokerLogger = null
// Validate required env var
let isExistingEnvVar = Promise.method(() => {
  _.each(config, (value, key) => {
    if (_.isUndefined(value)) throw new Error(`Undefined Env Var ${key}.`)
  })
  return true
})

let fifoPathDiff = Promise.method(() => {
  if (config.FIFO_PATH_LOG === config.FIFO_PATH_EXCEPTION_LOG) throw new Error(`Env Var FIFO_PATH_LOG and FIFO_PATH_EXCEPTION_LOG must not be same.`)
  else return true
})

function watchLog(file, type) {
  fs.open(file, fs.constants.O_RDONLY | fs.constants.O_NONBLOCK, (err, fd) => {
    let pipe = new net.Socket({ fd })
    let data = Buffer.from('')
    pipe.on('data', (chunk) => {
      data = Buffer.concat([data, chunk])
    })
    pipe.on('close', () => {
      watchLog(file, type)
    })
    pipe.on('end', () => {
      data = data.toString()
      if (type === 'log') brokerLogger.log(data)
      if (type === 'exception') brokerLogger.logException(new Error(data))
    })
  })
  return true
}

isExistingEnvVar()
  .then(() =>  fifoPathDiff())
  .then(() => {
    if (!_.isUndefined(process.env.LOGGERS)) config.LOGGERS = process.env.LOGGERS
    if (!_.isUndefined(process.env.EXCEPTION_LOGGERS)) config.EXCEPTION_LOGGERS = process.env.EXCEPTION_LOGGERS

    brokerLogger = new BrokerLogger(config)
    return brokerLogger.once('ready', () => {
      watchLog(config.FIFO_PATH_LOG, 'log')
      watchLog(config.FIFO_PATH_EXCEPTION_LOG, 'exception')
      return true
    })
  })
  .catch((err) => {throw err})

FROM emqx/emqx:v4.0.0
LABEL maintainer="Reekoh"

COPY . /opt

USER root

RUN apk update && apk add nodejs
RUN apk add npm
RUN npm install

USER emqx

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]

CMD run.sh
